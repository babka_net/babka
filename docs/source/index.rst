.. Babka documentation master file, created by
   sphinx-quickstart on Thu Dec 15 21:25:04 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Babka's documentation!
=================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   usage/activitycolander.rst



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
