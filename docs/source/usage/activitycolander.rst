################
ActivityColander
################
Spam filter for ActivityPub
===========================

ActivityPub is a federated protocol using json-ld. Servers pass messages as POST requests between them.

ActivityColander intercepts these requests after they passed through a reverse proxy handling SSL certificates and encryption, it inspects the contents of the request and either passes it on to the ActivityPub server, marks it with some additional machine-readable information and passes it on  or blocks the request entirely.

ActivityColander parses a request and if the request is an Activity (the messages passed between servers by ActivityPub), it is passed through a configurable pipeline of checks. These checks return both a `score` value between 0 and 1 and a `note` that can contain any human readable message.

The `score` is weighted according to the preferences of the server administrator. After all scores have been computed and weighted, they are summed and divided by the maximum possible score in that pipeline and this value is compared to two thresholds.

First if the resulting value exceeds a blocking threshold, it is blocked and otherwise if it exceeds a spam threshold, it is marked spam and passed on.

If neither threshold is exceeded, the request is simply passed on.

Setup with docker-compose using Mastodon +Babka
===============================================
The simplest possible setup is using Babka's own Mastodon fork in docker-compose.

On this branch we already set up an `activitycolander` service that sits before mastodon itself.

.. code-block::
   :caption: Using Mastodon +Babka fork with docker-compose

   git clone https://gitlab.com/babka_net/mastodon-babka
   cd mastodon-babka
   docker-compose up -d

Configuration
-------------
All configuration for ActivityColander in this set up can be found in the `activitycolander` folder.

This folder contains these files:

================== ===========
Filename           Description
================== ===========
check_pipeline.lua Customize the pipeline used by ActivityColander
config.lua         General configuration of ActivityColander
domains.txt        When DomainCheck is used, the domains in this file are checked
words.txt          When WholeWordCheck is used, the words in this file are checked
================== ===========

The `txt` files are newline seperated values and lines beginning with `#` are comments.
And the `lua` files are executed as valid lua code and each value is explictely documented.

Using the ActivityColander image alone
======================================
Using ActivityColander with our pre-configured `docker-compose.yml` file is by far easiest way to use ActivityColander, but is also by far the most restrictive.

ActivityColander does not make assumptions about the ActivityPub server it sits before, so you should be able to use ActivityColander for both Mastodon, GoToSocial, Pleroma, Misskey and any other implementation of the ActivityPub protocol.

But to use ActivityColander with these projects, you'll have to configure the ActivityColander image yourself.

This table describes where to put the files described in the table above in the ActivityColander image:

===================================== ===============================
Path when using docker-compose        Path when using the image alone
===================================== ===============================
`activitycolander/config.lua`         `/usr/local/openresty/nginx/activitycolander/config.lua`
`activitycolander/check_pipeline.lua` `/usr/local/openresty/nginx/activitycolander/check_pipeline.lua`
`activitycolander/words.txt`          `/config/words.txt`
`activitycolander/domains.txt`        `/config/domains.txt`
===================================== ===============================

When you want to configure the ActivityColander image, you'll need to overwrite the paths in the right column.
See the above section for a description of the individual files.

Writing your own Check
======================
The number of checks provided by Babka is limited, but it is very easy to write your own checks for ActivityColander.

Setting up ActivityColander with a custom check
-----------------------------------------------
